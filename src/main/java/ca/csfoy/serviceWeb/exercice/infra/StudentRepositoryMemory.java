package ca.csfoy.serviceWeb.exercice.infra;

import java.util.List;

import ca.csfoy.serviceWeb.exercice.domain.Student;
import ca.csfoy.serviceWeb.exercice.domain.StudentRepository;
import ca.csfoy.serviceWeb.exercice.domain.error.UserIdGivenDoesntExistInContextException;

public class StudentRepositoryMemory implements StudentRepository {
    
    private final StudentDao dao;
    
    public StudentRepositoryMemory(StudentDao dao) {
        this.dao = dao;
        this.dataSeeder();
    }
    
    private void dataSeeder() {
        dao.insert(new Student("Tested0", "Johnny", 2001, 2002));
        dao.insert(new Student("Tested1", "Johnny", 2001, 2002));
        dao.insert(new Student("Tested2", "Johnny", 2001, 2002));
    }
    
    @Override
    public Student getById(String id) {
        if (dao.doesExist(id)) {
            return dao.selectById(id);
        }
        throw new UserIdGivenDoesntExistInContextException("Id does not exist");
    }

    @Override
    public List<Student> getAll() {
        return dao.selectAll();
    }

    @Override
    public void create(Student student) {
        dao.insert(student);
    }

    @Override
    public void modify(String id, Student event) {
        if (dao.doesExist(id)) {
            dao.update(id, event);
        }
        throw new UserIdGivenDoesntExistInContextException("Id does not exist");
    }

    @Override
    public void delete(String id) {
        if (dao.doesExist(id)) {
            dao.delete(id);
        }
        throw new UserIdGivenDoesntExistInContextException("Id does not exist");
    }

}
