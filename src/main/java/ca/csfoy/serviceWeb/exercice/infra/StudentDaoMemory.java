package ca.csfoy.serviceWeb.exercice.infra;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ca.csfoy.serviceWeb.exercice.domain.Student;
import ca.csfoy.serviceWeb.exercice.domain.error.StudentAlreadyExistException;

public class StudentDaoMemory implements StudentDao {

    private HashMap<String, Student> list; 
    
    public StudentDaoMemory() {
        this.list = new HashMap<String, Student>(); 
        this.dataSeeder();
    }
    
    private void dataSeeder() {
        this.list.put("TJ0", new Student("Tested0", "Johnny", 2001, 2002));
        this.list.put("TJ1", new Student("Tested1", "Johnny", 2001, 2002));
        this.list.put("TJ2", new Student("Tested2", "Johnny", 2001, 2002));
    }
    
    @Override
    public Student selectById(String id) {
        return list.get(id);
    }

    @Override
    public List<Student> selectAll() {
        return new ArrayList<Student>(list.values());
    }

    @Override
    public Student insert(Student student) {
        if (!studentValuesMatchesAnother(student)) {
            this.list.put(student.getId(), student);
            return student;
        }
        throw new StudentAlreadyExistException("The student given already exist!");
    }

    @Override
    public void update(String id, Student event) {
        this.list.replace(id, event);
    }

    @Override
    public void delete(String id) {
        this.list.remove(id);
    }

    @Override
    public boolean doesExist(String id) {
        return this.list.containsKey(id);
    }
    
    private boolean studentValuesMatchesAnother(Student student) {
        for (Student studentInList : this.list.values()) {
            if (student.getFirstname().equals(studentInList.getFirstname()) && 
                student.getLastname().equals(studentInList.getLastname()) &&
                student.getBithYear() == studentInList.getBithYear()) {
                return true;
            }
        }
        return false;
    }

}
