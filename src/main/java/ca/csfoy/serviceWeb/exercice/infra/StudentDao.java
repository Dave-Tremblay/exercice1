package ca.csfoy.serviceWeb.exercice.infra;

import ca.csfoy.serviceWeb.exercice.domain.Student;

public interface StudentDao extends Dao<Student, String> {

}
