package ca.csfoy.serviceWeb.exercice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import ca.csfoy.serviceWeb.exercice.api.HealthResource;
import ca.csfoy.serviceWeb.exercice.api.StudentResource;
import ca.csfoy.serviceWeb.exercice.controller.HealthController;
import ca.csfoy.serviceWeb.exercice.controller.StudentController;
import ca.csfoy.serviceWeb.exercice.controller.StudentConverter;
import ca.csfoy.serviceWeb.exercice.infra.StudentDao;
import ca.csfoy.serviceWeb.exercice.infra.StudentDaoMemory;
import ca.csfoy.serviceWeb.exercice.infra.StudentRepositoryMemory;

@Configuration
public class MainContent {

    @Bean
    public HealthResource getHealthResource() {
        return new HealthController();
    }
    
    @Bean
    public StudentResource getStudentResource() {
        return new StudentController(getStudentConverter(), getStudentRepositoryMemory());
    }
    
    @Bean
    public StudentConverter getStudentConverter() {
        return new StudentConverter();
    }
    
    @Bean
    public StudentRepositoryMemory getStudentRepositoryMemory() {
        return new StudentRepositoryMemory(getStudentDao());
    }
    
    @Bean
    public StudentDao getStudentDao() {
        return new StudentDaoMemory();
    }
}
