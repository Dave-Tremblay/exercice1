package ca.csfoy.serviceWeb.exercice.controller;

import ca.csfoy.serviceWeb.exercice.api.HealthResource;

public class HealthController implements HealthResource {

    @Override
    public String getHealth() {
        return "pong";
    }

}
