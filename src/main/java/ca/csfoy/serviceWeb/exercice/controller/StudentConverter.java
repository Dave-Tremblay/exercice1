package ca.csfoy.serviceWeb.exercice.controller;

import java.util.ArrayList;
import java.util.List;

import ca.csfoy.serviceWeb.exercice.api.StudentDtoForCreate;
import ca.csfoy.serviceWeb.exercice.api.StudentDtoForGeneral;
import ca.csfoy.serviceWeb.exercice.domain.Student;

public class StudentConverter {
    
    public StudentDtoForGeneral studentToGeneral(Student student) {
        return new StudentDtoForGeneral(student.getId(), student.getLastname(), student.getFirstname());
    }
    
    public Student studentDtoToStudent(StudentDtoForCreate student) {
        return new Student(student.lastname, student.firstname, student.birthYear, student.joinedYear);
    }
    
    public Student studentModifyDtoToStudent(String id, StudentDtoForCreate student) {
        return new Student(id, student.lastname, student.firstname, student.birthYear, student.joinedYear);
    }
    
    
    public StudentDtoForCreate studentToDto(Student student) {
        return new StudentDtoForCreate(student.getId(), student.getFirstname(), student.getLastname(), student.getBithYear(), student.getJoinedYear());
    }
    
    public List<StudentDtoForGeneral> studentListToStudentDtoGeneralList(List<Student> source) {
        List<StudentDtoForGeneral> result = new ArrayList<>();
        for (Student student : source) {
            result.add(this.studentToGeneral(student));
        }
        return result;
    }
}
