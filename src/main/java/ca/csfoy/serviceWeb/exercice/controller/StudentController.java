package ca.csfoy.serviceWeb.exercice.controller;

import java.util.List;

import ca.csfoy.serviceWeb.exercice.domain.StudentRepository;
import ca.csfoy.serviceWeb.exercice.api.StudentDtoForCreate;
import ca.csfoy.serviceWeb.exercice.api.StudentDtoForGeneral;
import ca.csfoy.serviceWeb.exercice.api.StudentResource;

public class StudentController implements StudentResource {
    
    private StudentConverter converter;
    private StudentRepository repo;
    
    public StudentController(StudentConverter converter, StudentRepository repo ) {
        this.converter = converter;
        this.repo = repo;
    }
    
    @Override
    public void createStudent(StudentDtoForCreate student) {
        System.out.println("ID: " + student.studentID + " Name: " + student.firstname);
        this.repo.create(converter.studentDtoToStudent(student));
    } 
    
    @Override
    public List<StudentDtoForGeneral> getAllStudents() {
        return converter.studentListToStudentDtoGeneralList(repo.getAll());
    }

    @Override
    public StudentDtoForCreate getAStudent(String id) {
        return converter.studentToDto(repo.getById(id));
    }

    @Override
    public void modifyAStudent(String id, StudentDtoForCreate student) {
        this.repo.modify(id, converter.studentModifyDtoToStudent(id, student));
    }

    @Override
    public void deleteAStudent(String id) {
        this.repo.delete(id);
    }
}
