package ca.csfoy.serviceWeb.exercice.controller;

import java.time.LocalDateTime;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import ca.csfoy.serviceWeb.exercice.api.ErrorMessageDto;
import ca.csfoy.serviceWeb.exercice.domain.error.StudentAlreadyExistException;
import ca.csfoy.serviceWeb.exercice.domain.error.UserIdGivenDoesntExistInContextException;

@ControllerAdvice
@ResponseBody
public class ErrorHandler {

    private Logger logger = LoggerFactory.getLogger(ErrorHandler.class);
    
    @ExceptionHandler(UserIdGivenDoesntExistInContextException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public ErrorMessageDto studentNotFoundException(UserIdGivenDoesntExistInContextException exception) {
        String errorId = exception.hashCode() + "";
        logger.error(LocalDateTime.now().toString() 
                + " [" + errorId + "]: " 
                + exception.getMessage() 
                + ExceptionUtils.getStackTrace(exception));
        return new ErrorMessageDto(LocalDateTime.now(), errorId, exception.getMessage());
    }
    
    @ExceptionHandler(StudentAlreadyExistException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public ErrorMessageDto studentAlreadyExistException(StudentAlreadyExistException exception) {
        String errorId = exception.hashCode() + "";
        logger.error(LocalDateTime.now().toString() 
                + " [" + errorId + "]: " 
                + exception.getMessage() 
                + ExceptionUtils.getStackTrace(exception));
        return new ErrorMessageDto(LocalDateTime.now(), errorId, exception.getMessage());
    }
}
