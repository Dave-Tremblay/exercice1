package ca.csfoy.serviceWeb.exercice.api;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping(value = HealthResource.HEALTH_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface HealthResource {
    
    String HEALTH_PATH = "/health";
   
    @Operation(summary = "Checks Api status")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Api status code",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = HealthResource.class)) }) })
           
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    String getHealth();
    
}
