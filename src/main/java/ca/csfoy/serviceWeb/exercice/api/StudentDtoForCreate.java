package ca.csfoy.serviceWeb.exercice.api;

public class StudentDtoForCreate {
    public final String studentID;
    public final String lastname;
    public final String firstname;
    public final int birthYear;
    public final int joinedYear; 
    
    public StudentDtoForCreate(String id, String last, String first, int birth, int joined) {
        this.studentID = id;
        this.lastname = last;
        this.firstname = first;
        this.birthYear = birth;
        this.joinedYear = joined;
    }
}
