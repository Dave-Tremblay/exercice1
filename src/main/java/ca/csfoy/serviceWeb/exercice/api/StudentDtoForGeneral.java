package ca.csfoy.serviceWeb.exercice.api;

public class StudentDtoForGeneral {
    public String studentID;
    public String lastname;
    public String firstname;
    
    public StudentDtoForGeneral(String id, String last, String first) {
        this.studentID = id;
        this.lastname = last;
        this.firstname = first;
    }
}