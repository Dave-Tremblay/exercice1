package ca.csfoy.serviceWeb.exercice.api;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;

@RequestMapping(value = StudentResource.STUDENT_PATH, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@ResponseBody
public interface StudentResource {

    String STUDENT_PATH = "/student";
    //String CHANGES_PATH = "/";
    
    String PARAM_ID = "id";
    
    String PATH_WITH_ID = "/{" + PARAM_ID + "}";
    
    @Operation(summary = "Create a new student")
    @ApiResponses(value = { @ApiResponse(responseCode = "201",
            description = "Created a new student",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StudentResource.class)) }) })
    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    void createStudent(@RequestBody StudentDtoForCreate student);
    
    @Operation(summary = "Get all students")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Got all students",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StudentResource.class)) }) })
    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    List<StudentDtoForGeneral> getAllStudents();
    
    @Operation(summary = "Get a student by id")
    @ApiResponses(value = { @ApiResponse(responseCode = "200",
            description = "Got a student",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StudentResource.class)) }) })
    @GetMapping(StudentResource.PATH_WITH_ID)
    @ResponseStatus(HttpStatus.OK)
    StudentDtoForCreate getAStudent(@PathVariable(PARAM_ID) String id);

    @Operation(summary = "Modify a student by id")
    @ApiResponses(value = { @ApiResponse(responseCode = "204",
            description = "Modified a student",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StudentResource.class)) }) })
    @PutMapping(StudentResource.PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void modifyAStudent(@PathVariable(PARAM_ID) String id, @RequestBody StudentDtoForCreate student);

    @Operation(summary = "Delete a student by id")
    @ApiResponses(value = { @ApiResponse(responseCode = "204",
            description = "Delete a student",
            content = {@Content(mediaType = "application/json", schema = @Schema(implementation = StudentResource.class)) }) })
    @DeleteMapping(StudentResource.PATH_WITH_ID)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteAStudent(@PathVariable(PARAM_ID) String id);
}
