package ca.csfoy.serviceWeb.exercice.api;

import java.time.LocalDateTime;

public class ErrorMessageDto {

    public final LocalDateTime time;
    public final String errorId;
    public final String errorMsg;
    
    public ErrorMessageDto(LocalDateTime time, String errorId, String errorMsg) {
        this.time = time;
        this.errorId = errorId;
        this.errorMsg = errorMsg;
    }
}
