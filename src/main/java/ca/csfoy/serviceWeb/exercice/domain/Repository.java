package ca.csfoy.serviceWeb.exercice.domain;

import java.util.List;

/*
 * Interface générique de repository pour les opérations CRUD (create, read, update, delete).  Notez le vocabulaire de 'haut niveau'.
 */
public interface Repository<T, U> {

    T getById(U id);

    List<T> getAll();

    void create(T event);

    void modify(U id, T event);

    void delete(U id);
}
