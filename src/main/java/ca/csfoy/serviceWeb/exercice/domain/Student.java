package ca.csfoy.serviceWeb.exercice.domain;

public class Student {
    private static int idNumber = 0;
    
    private String studentID;
    private String lastname;
    private String firstname;
    private int birthYear;
    private int joinedYear; 

    public Student(String last, String first, int birth, int joined) {
        this.studentID = "" + last.charAt(0) + first.charAt(0) + String.valueOf(idNumber);
        this.lastname = last;
        this.firstname = first;
        this.birthYear = birth;
        this.joinedYear = joined;
        
        idNumber++;
    }
    
    public Student(String id, String last, String first, int birth, int joined) {
        this.studentID = id;
        this.lastname = last;
        this.firstname = first;
        this.birthYear = birth;
        this.joinedYear = joined;
    }
    
    public String getId() {
        return this.studentID;
    }
    
    public String getFirstname() {
        return this.firstname;
    }
    
    public String getLastname() {
        return this.lastname;
    }
    
    public int getBithYear() {
        return this.birthYear;
    }
    
    public int getJoinedYear() {
        return this.joinedYear;
    }
}
