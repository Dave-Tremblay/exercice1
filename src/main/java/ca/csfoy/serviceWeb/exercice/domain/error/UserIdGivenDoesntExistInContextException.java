package ca.csfoy.serviceWeb.exercice.domain.error;

public class UserIdGivenDoesntExistInContextException extends RuntimeException {
    
    private static final long serialVersionUID = 1L;

    public UserIdGivenDoesntExistInContextException(String message) {
        super(message);
    }
}
